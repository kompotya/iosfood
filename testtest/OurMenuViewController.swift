//
//  OurMenuViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 18.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import SDWebImage

class OurMenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    @IBOutlet weak var CollectionView: UICollectionView!
    
    var categoriesModel = [CategoryModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doGetRequest()
        setBackgroundImage() 
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesModel.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_view_cell_our_menu", for: indexPath) as! CollectionViewCellOurMenu
        if categoriesModel.count != 0 {
            cell.titleCategory.text = self.categoriesModel[indexPath.row].mTitleCategory
            cell.descriptionCategory.text = self.categoriesModel[indexPath.row].mDescriptionCategory
            
         
            
            let url = URL(string: self.categoriesModel[indexPath.row].mImageCategory)
            
            cell.imageCategory.sd_setImage(with: url)
            cell.imageCategory.setShowActivityIndicator(true)
            cell.imageCategory.setIndicatorStyle(.gray)
        }
        
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dishesViewController = self.storyboard?.instantiateViewController(withIdentifier:
            "DishesViewController") as! DishesViewController
        dishesViewController.categoryID = categoriesModel[indexPath.row].mIdCategory
        dishesViewController.categoryName = categoriesModel[indexPath.row].mTitleCategory
        self.navigationController?.pushViewController(dishesViewController, animated: true)
    }
    
    
    func doGetRequest () ->Void{
        let urlString = Constants.API_URL_CATEGORIES
        
        guard let url  = URL(string: urlString ) else {
            print("error convert to url")
            return;
        }
        
        let urlRequest = URLRequest(url: url)
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [AnyObject] else {
                        print("error trying to convert data to JSON")
                        return
                }
                for category in todo {
                    
                    self.categoriesModel.append(CategoryModel(title: category["title"] as! String, description: category["description"] as! String, image: category["img"] as! String, id: (category["category"] as! String)))
                
                }
                self.showResult()
                
                
            }
                
            catch  {
                print("error trying to convert data to JSON")
                return
            }
            
        }
        task.resume()
    }
    func showResult()-> Void {
        
        DispatchQueue.main.async {
            self.CollectionView.reloadData()
        }
        
        
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
