//
//  CollectionViewCellCoupons.swift
//  testtest
//
//  Created by Michael Ovsienko on 20.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellCoupons: UICollectionViewCell {
    
    @IBOutlet weak var imageCoupon: UIImageView!
    @IBOutlet weak var titleCoupon: UILabel!
    @IBOutlet weak var descriptionCoupon: UILabel!
    @IBOutlet weak var expDateCoupon: UILabel!
    @IBOutlet weak var codeCoupon: UILabel!
}
