//
//  DishesViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 19.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Toaster

class DishesViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    // MARK: - @IBOutlets
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    
    // MARK: - Properties
    
    var categoryID : String = ""
    var categoryName : String = ""
    var dishesModel = [DishesModel]()
    var selectedIndexPath: IndexPath?
    var selectedDish : Int = -1
    var selectedDishCount : Int = 0
    
    // MARK : - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setBackgroundImage()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        doGetRequest()
        
    }
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.title = categoryName
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  dishesModel.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_view_cell_dishes", for: indexPath) as! CollectionViewCellDishes
        cell.descriptionDish.text = dishesModel[indexPath.row].descriptionDish
        cell.titleDish.text = dishesModel[indexPath.row].titleDish
        cell.priceDish.text = "₪" + String(format: "%.2f",dishesModel[indexPath.row].priceDish)
        
        let image = self.dishesModel[indexPath.item].imageDish.replacingOccurrences(of: " ", with: "%20")
        if image == "" {
            cell.imageDish.image = UIImage(named: "slider-image")
        } else {
            let url = URL(string: image)
            cell.imageDish.setShowActivityIndicator(true)
            cell.imageDish.setIndicatorStyle(.gray)
            cell.imageDish.sd_setImage(with: url)
            
        }
        
        cell.addToCartButton.setTitle("deal_item_add_to_cart".localized(), for: .normal)
        cell.addToCartButton.layer.cornerRadius = 5
        cell.addToCartButton.addTarget(self, action: #selector(buttonActionAddToCart), for: .touchUpInside)
        return cell
        
    }
    
    func buttonActionAddToCart (sender: UIButton!){
        let cell : CollectionViewCellDishes = sender.superview?.superview as! CollectionViewCellDishes
        let indexPath = collectionView.indexPath(for: cell)
        let item = BasketItem(id: dishesModel[(indexPath?.row)!].idDish,image: cell.imageDish.image!, title: cell.titleDish.text!, description: cell.descriptionDish.text!, price: dishesModel[(indexPath?.row)!].priceDish, count : 1)
        if Singleton.sharedInstance.listBasketItems.count == 0 {
            Singleton.sharedInstance.listBasketItems.append(item)
            
        } else {
            var founded : Bool = false
            for basketItem in Singleton.sharedInstance.listBasketItems {
                if basketItem.idItem == dishesModel[(indexPath?.row)!].idDish {
                    basketItem.countItem = basketItem.countItem + 1
                    founded = true
                }
                
            }
            if !founded {
                Singleton.sharedInstance.listBasketItems.append(item)
            }
        }
        
        Toast(text: "deal_added".localized(), duration: 0.5).show()

        self.tabBarController?.tabBar.items![4].badgeValue = String(Singleton.sharedInstance.listBasketItems.count)
        
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doGetRequest () ->Void{
        let parameters: Parameters = ["categoryId": categoryID]
        
        Alamofire.request(Constants.API_URL_DISHES, method: .get, parameters: parameters).responseJSON {response  in
            let jsonResponse = (response.result.value) as? [AnyObject]
            
            for dish in jsonResponse! {
                let title = dish["title"] as! String
                //print(title)
                var price : Double = 0
                var image : String = ""
                var description : String = ""
                
                if   dish["description"] as? String  == nil   {
                    let ddls = dish["ddls"] as? [AnyObject]
                    description = ddls?[0]["description"] as! String
                } else {
                    description = (dish["description"] as? String)!
                }
                
                if dish["priceBase"] as? Double == nil {
                    let sizes = dish["sizes"] as? [AnyObject]
                    price = sizes?[0]["price"] as! Double
                } else {
                    price = (dish["priceBase"] as? Double)!
                }
                if dish["img1x"] as? String != nil {
                    image = (dish["img1x"] as? String)!
                } else {
                    let sizes = dish["sizes"] as? [AnyObject]
                    image = sizes?[0]["img1x"] as! String
                }
                
                self.dishesModel.append(DishesModel(id: (dish["id"] as? Int)!, title: title, description: description, price: price, image: image))
                
            }
            self.collectionView.reloadData()
            //  self.CollectionView.reloadData()
            
        }
        
        
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    
}

