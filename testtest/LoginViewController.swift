//
//  ThreeDealItemViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 14.02.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
import NVActivityIndicatorView

class LoginViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable {

    //MARK: - IBOutlets
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var loadGrayView: UIView!
    //MARK: - Variables
    var dealsModel = [ThreeDealModel]()
    var activityIndicatorView : NVActivityIndicatorView? = nil

    
    //MARK: - Lifecycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadGrayView.isHidden = true
        setBackgroundImage()
        
        let wigth = self.view.frame.width * 0.2
        let heigth = self.view.frame.height * 0.2
        let frame = CGRect(x: self.view.center.x - wigth/2.0 , y: self.view.center.y - heigth/2.0, width: wigth, height: heigth)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballTrianglePath, color: .white)
        phoneTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.loginButton.layer.cornerRadius = 3
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }
   
    @IBAction func getUserDataByPhone(_ sender: Any) {
        self.dismissKeyboard()
        self.showAnimation()
        if phoneTextField.text?.characters.count == 10 {
            var phoneNumber : String = (phoneTextField.text?.replacingOccurrences(of: "-", with: ""))!
            
            phoneNumber = String(phoneNumber.characters.dropFirst())
            phoneNumber = "972" + phoneNumber
            
            let parameters: Parameters = ["number": Int64(phoneNumber)]
            Alamofire.request(Constants.API_UTL_CUSTOMER_BY_PHONE, method: .get, parameters: parameters).responseJSON{ response in
                switch response.result{
                case .success:
                    let statusCode: Int! = response.response?.statusCode
                    switch statusCode{
                    case 200:
                        let jsonResponse = (response.result.value) as? [String:AnyObject]
                        var phoneNumber = jsonResponse?["phone"] as? String
                        let idCustomer = jsonResponse?["id"] as? Int
                        phoneNumber = phoneNumber?.substring(from: (phoneNumber?.characters.index((phoneNumber?.startIndex)!, offsetBy: 3))!)
                        phoneNumber = "0" + phoneNumber!
                        let preferences = UserDefaults.standard
                        preferences.set(phoneNumber, forKey: "phoneNumber")
                        preferences.set(idCustomer, forKey: "customerId")
                        
                        self.doGetRequest()
                        

                    break
                    case 400:
                        self.stopAnimation()
                        Toast(text: "fields_required".localized(), duration: Delay.short).show()
                        break
                    case 404:
                        self.stopAnimation()
                        Toast(text: "no_user_found".localized(), duration: Delay.short).show()
                        break
                    default :
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
        } else {
            self.stopAnimation()
            Toast(text: "invalid_phone".localized(), duration: Delay.short).show()

        }
    }
    //MARK: - Other functions
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    func textFieldDidChange(_ textField: UITextField) {
       
        if (textField.text?.characters.count)! > 10 {
            let truncated = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            textField.text = truncated
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.layer.position.y > self.view.center.y {
            scrollView.setContentOffset(CGPoint(x: 0,y: 250), animated: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
    }
    func showAnimation () -> Void {
        loadGrayView.isHidden = false
        self.view.addSubview(activityIndicatorView!)
        activityIndicatorView?.startAnimating()
    }
    func stopAnimation() -> Void {
        loadGrayView.isHidden = true
        activityIndicatorView?.stopAnimating()
        self.scrollView.backgroundColor = .clear
        

    }
    
    func doGetRequest () -> Void{
        
        Alamofire.request(Constants.API_URL_THREE_DEALS, method: .get, parameters: nil).responseJSON { (response) in
            var image : String;
            let jsonResponse = (response.result.value) as? [String:AnyObject]
            
            if let bestDeal = jsonResponse?["bestDeal"] as? [String:AnyObject]{
                let title = bestDeal["title"] as? String
                let description = bestDeal["descr"] as? String
                var productName = bestDeal["productName"] as? String
                productName = productName?.replacingOccurrences(of: "<span>", with: "")
                productName = productName?.replacingOccurrences(of: "</span>", with: "")
                let price = bestDeal["price"] as? Double
                let productNameSecondary = bestDeal["productNameSecondary"] as? String
                image = (bestDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: (bestDeal["id"] as? Int)!,imageDeal: image, titleDeal: title!, descriptionDeal: description!, priceDeal: price!, productNameDeal :productName!, productNameSecondaryDeal: productNameSecondary!))
            }
            if let contextDeal = jsonResponse?["contextDeal"] as? [String:AnyObject]{
                let title = contextDeal["title"] as? String
                let description = contextDeal["descr"] as? String
                let productName = contextDeal["productName"] as? String
                let price = contextDeal["price"] as? Double
                let productNameSecondary = contextDeal["productNameSecondary"] as? String
                image = (contextDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: contextDeal["id"] as! Int,imageDeal: image, titleDeal: title!, descriptionDeal: description!, priceDeal: price!, productNameDeal :productName!, productNameSecondaryDeal: productNameSecondary!))
            }
            if let todaysDeal = jsonResponse?["todaysDeal"] as? [String:AnyObject]{
                let title = todaysDeal["title"] as? String
                let description = ""
                var productNameToday = todaysDeal["productName"] as? String
                productNameToday = productNameToday?.replacingOccurrences(of: "<span>", with: "")
                productNameToday = productNameToday?.replacingOccurrences(of: "</span>", with: "")
                
                let price = todaysDeal["price"] as? Double
                let productNameSecondary = todaysDeal["productNameSecondary"] as? String
                image = (todaysDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: todaysDeal["id"] as! Int,imageDeal: image, titleDeal: title!, descriptionDeal: description, priceDeal: price!, productNameDeal :productNameToday!, productNameSecondaryDeal: productNameSecondary!))
            }
            
            self.stopAnimation()

            Singleton.sharedInstance.listModelsData = self.dealsModel
            let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabConttrollerViewController")
            
            self.present(tabViewController!, animated: true, completion: nil)
            
        }
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    func initializeData () {
        self.phoneTextField.placeholder = "phone_textfield_login".localized()
        self.phoneLabel.text = "phone_label_login".localized()
        self.loginButton.titleLabel?.text = "button_login".localized()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
