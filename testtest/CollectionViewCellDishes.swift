//
//  CollectionViewCellDishes.swift
//  testtest
//
//  Created by Michael Ovsienko on 19.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellDishes: UICollectionViewCell {
    
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var imageDish: UIImageView!
    @IBOutlet weak var titleDish: UILabel!
    @IBOutlet weak var priceDish: UILabel!
    @IBOutlet weak var descriptionDish: UILabel!
    @IBOutlet weak var countStepper: UIStepper!
   
   
}
