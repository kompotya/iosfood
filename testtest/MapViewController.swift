//
//  MapViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 24.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 32.109333,longitude: 34.855499, zoom: 6)
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 100, height: 100))
        let mapView = GMSMapView.map(withFrame: rect, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.zoomGestures = true
        self.view = mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(32.109333
, 34.855499)
        marker.title = "Tel-Aviv"
        marker.icon = UIImage(named: "marker")
        marker.snippet = "Israel"
        marker.map = mapView
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeData (){
        self.title = "title_locator".localized()
        self.tabBarItem.title = "title_locator".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
