//
//  Singleton.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Singleton {
    var listModelsData = [ThreeDealModel]()
    var listBasketItems = [BasketItem]()
    var customerData : Customer? = nil
    static let sharedInstance = Singleton()
    private init(){}
}
