//
//  TableViewCellDishes.swift
//  testtest
//
//  Created by Michael Ovsienko on 31.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class TableViewCellDishes: UITableViewCell {

    @IBOutlet weak var imageDish: UIImageView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var countValue: UIStepper!
    @IBOutlet weak var countItemDish: UILabel!
    @IBOutlet weak var titleDish: UILabel!
    @IBOutlet weak var descriptionDish: UILabel!
    @IBOutlet weak var priceDish: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        countValue.value = 0.0
//    }

}
