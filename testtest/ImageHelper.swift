//
//  ImageHelper.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class ImageHelper {
    static func loadImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: {
                    view.image = UIImage(data: data)
                })
            }
        }
        
        // Run task
        task.resume()
    }
}
