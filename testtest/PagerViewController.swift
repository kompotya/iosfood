//
//  PagerViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 09.02.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PagerViewController: ButtonBarPagerTabStripViewController {
    
    var isReload = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        // set up style before super view did load is executed
        // -
        self.settings.style.buttonBarBackgroundColor = .clear
        self.settings.style.buttonBarItemBackgroundColor = .clear
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 14)
        self.settings.style.buttonBarMinimumLineSpacing = 0
        
        // self.settings.style.selectedBarBackgroundColor = .black
        
        // self.containerView.backgroundColor = .red
        super.viewDidLoad()
        self.buttonBarView.selectedBar.backgroundColor = .white
        // self.buttonBarView.backgroundColor = .clear
        setBackgroundImage()
        self.moveToViewController(at: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }
    // MARK: - PagerTabStripDataSource
    
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController")
        let child_2 = self.storyboard?.instantiateViewController(withIdentifier: "PersonalZoneViewController")
        let child_3 = self.storyboard?.instantiateViewController(withIdentifier: "CouponsViewController")
        guard isReload else {
            return [child_1!, child_2!,child_3!]
        }
        
        var childViewControllers = [child_1, child_2,child_3]
        
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                swap(&childViewControllers[index], &childViewControllers[n])
            }
        }
        let nItems = 1 + (arc4random() % 2)
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
    }
    
    override func reloadPagerTabStripView() {
        isReload = true
        if arc4random() % 2 == 0 {
            pagerBehaviour = .progressive(skipIntermediateViewControllers: arc4random() % 2 == 0, elasticIndicatorLimit: arc4random() % 2 == 0 )
        }
        else {
            pagerBehaviour = .common(skipIntermediateViewControllers: arc4random() % 2 == 0)
        }
        super.reloadPagerTabStripView()
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    func initializeData() -> Void {
        self.tabBarItem.title = "account_title".localized()
        self.title = "account_title".localized()
    }
   
   
}
