//
//  DealItemViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 09.02.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Toaster
class DealItemViewController: UIViewController {

    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var priceDealItem: UILabel!
    @IBOutlet weak var descriptionDealItem: UILabel!
    @IBOutlet weak var titleDealItem: UILabel!
    @IBOutlet weak var imageDealItem: UIImageView!
    
    var deal : DealsModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundImage()
        self.addToCart.layer.cornerRadius = 5
        if deal != nil {
            priceDealItem.text = "₪" + String(format: "%.2f", (deal?.mPriceDeal)!)
            descriptionDealItem.text = deal?.mDescriptionDeal
            titleDealItem.text = deal?.mTitleDeal
            imageDealItem.setShowActivityIndicator(true)
            imageDealItem.setIndicatorStyle(.gray)
            let url = URL(string: (deal?.mImageDeal)!)
            imageDealItem.sd_setImage(with: url)
        }
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        addToCart.setTitle("deal_item_add_to_cart".localized(), for: .normal)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.title = deal?.mTitleDeal
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func addToCartButton(_ sender: Any) {
      
        let item = BasketItem(id: (deal?.mDealId)!,image: imageDealItem.image!, title: (deal?.mTitleDeal)!, description: (deal?.mDescriptionDeal)!, price:
            (deal?.mPriceDeal)!, count : 1)
        if Singleton.sharedInstance.listBasketItems.count == 0 {
            Singleton.sharedInstance.listBasketItems.append(item)
            
        } else {
            var founded : Bool = false
            for basketItem in Singleton.sharedInstance.listBasketItems {
                if basketItem.idItem == deal?.mDealId {
                    basketItem.countItem = basketItem.countItem + 1
                    founded = true
                }
                
            }
            
            if !founded {
                Singleton.sharedInstance.listBasketItems.append(item)
            }
        }       
        Toast(text: "deal_added".localized(), duration: 0.5).show()
        
        self.tabBarController?.tabBar.items![4].badgeValue = String(Singleton.sharedInstance.listBasketItems.count)
        
        

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
