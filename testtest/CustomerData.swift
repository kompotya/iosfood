//
//  CustomerData.swift
//  testtest
//
//  Created by Michael Ovsienko on 23.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Customer {
    var customerId : Int
    var fullName  : String
    var phone : String
    var email : String
    var address : String
    init(id: Int, name : String, phone : String, email : String, address : String) {
        self.customerId = id
        self.fullName = name
        self.phone = phone
        self.email = email
        self.address = address
    }
    func getParameters () -> [String:Any] {
        let parameters : [String:Any] = [
            "customerId": self.customerId,
            "newData": [
                "id": self.customerId,
                "fullName": self.fullName,
                "phone": self.phone,
                "email": self.email,
                "address": self.address
            ]
        ]
        return parameters
    }
}
