//
//  Dish.swift
//  testtest
//
//  Created by Michael Ovsienko on 19.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class DishesModel {
    var idDish : Int
    var titleDish : String
    var descriptionDish : String
    var priceDish : Double
    var imageDish : String
    init(id : Int, title : String, description :  String, price : Double, image : String) {
        self.idDish = id
        self.titleDish = title
        self.descriptionDish = description
        self.priceDish = price
        self.imageDish = image
    }
}
