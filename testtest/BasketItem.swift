//
//  BasketItem.swift
//  testtest
//
//  Created by Michael Ovsienko on 31.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class BasketItem: NSObject, NSCoding {
    var idItem : Int
    var imageItem : UIImage
    var titleItem : String
    var descriptionItem : String
    var priceItem : Double
    var countItem : Int
    init(id : Int, image : UIImage, title : String, description : String, price : Double, count : Int ) {
        self.idItem = id
        self.imageItem = image
        self.descriptionItem = description
        self.titleItem = title
        self.priceItem = price
        self.countItem = count
    }
    required init(coder decoder: NSCoder) {
        
        self.idItem = Int(decoder.decodeCInt(forKey: "id"))
        self.imageItem = decoder.decodeObject(forKey: "image") as! UIImage
        self.descriptionItem = decoder.decodeObject(forKey: "description") as! String
        self.titleItem = decoder.decodeObject(forKey: "title") as! String
        self.priceItem = decoder.decodeDouble(forKey: "price")
        self.countItem = Int(decoder.decodeCInt(forKey: "count"))
        
        
    }
    public func encode(with coder: NSCoder) {
        coder.encode(idItem, forKey: "id")
        coder.encode(imageItem, forKey: "image")
        coder.encode(titleItem, forKey: "description")
        coder.encode(descriptionItem, forKey: "title")
        coder.encode(priceItem, forKey: "price")
        coder.encode(countItem, forKey: "count")
        
    }
}
