//
//  File.swift
//  testtest
//
//  Created by Michael Ovsienko on 16.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation

class ContextDealModel {
    var mImageUrlDeal :String
    var mTitleDeal : String
    var mDescriptionDeal : String
    var mPriceDeal : Int
    var mProductName : String
    var mProductNameSecondary : String
    
    init(imageDeal : String, titleDeal : String, descriptionDeal : String, priceDeal : Int, productNameDeal : String ,productNameSecondaryDeal : String) {
        self.mImageUrlDeal = imageDeal
        self.mTitleDeal = titleDeal
        self.mDescriptionDeal = descriptionDeal
        self.mPriceDeal = priceDeal
        self.mProductName = productNameDeal
        self.mProductNameSecondary = productNameSecondaryDeal
    }
}
