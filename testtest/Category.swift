//
//  Category.swift
//  testtest
//
//  Created by Michael Ovsienko on 18.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class CategoryModel  {
    var mTitleCategory : String
    var mDescriptionCategory: String
    var mImageCategory : String
    var mIdCategory : String
    init(title : String, description : String, image : String, id : String   ) {
        self.mTitleCategory = title
        self.mDescriptionCategory = description
        self.mImageCategory = image
        self.mIdCategory = id
    }
}
