//
//  ViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 05.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow
import Localize_Swift

class ViewController: UIViewController    {
    
    @IBOutlet weak var dealView: UIView!
    @IBOutlet weak var titleDeal: UILabel!
    @IBOutlet weak var descriptionDeal: UILabel!
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var orderNowButton: UIButton!
    @IBOutlet weak var priceDeal: UILabel!
    
    @IBOutlet weak var feelingHungryLabel: UILabel!
    
    @IBOutlet weak var buttonShowDeals: UIButton!
    var dealsModel = [ThreeDealModel]()
    
    @IBAction func showDeals(_ sender: Any) {

        self.tabBarController?.selectedIndex = 2
        // let tabBarItem = self.tabBarController?.tabBar.items![0]
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        dealsModel  = Singleton.sharedInstance.listModelsData
        self.orderNowButton.layer.cornerRadius = 5
        setBackgroundImage()
        if Singleton.sharedInstance.listBasketItems != nil && Singleton.sharedInstance.listBasketItems.count != 0 {
        self.tabBarController?.tabBar.items![4].badgeValue = String(Singleton.sharedInstance.listBasketItems.count)
        }

        
        slideShow.slideshowInterval = 4.0
        slideShow.backgroundColor = UIColor.clear
        
        
        slideShow.pageControl.currentPageIndicatorTintColor = UIColor.red;
        slideShow.pageControl.pageIndicatorTintColor = UIColor.white;
        
        
        
        slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
    
        let sdWebImageSource = [SDWebImageSource(urlString: dealsModel[0].mImageUrlDeal)!, SDWebImageSource(urlString: dealsModel[1].mImageUrlDeal)!, SDWebImageSource(urlString: dealsModel[2].mImageUrlDeal)!]
        slideShow.setImageInputs(sdWebImageSource)
        // TODO: on slide user change text
        self.setDealsInfo()
        
        
        Timer.scheduledTimer(timeInterval: 0.1,
                             target: self,
                             selector: #selector(self.setDealsInfo),
                             userInfo: nil,
                             repeats: true)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onViewTapped(_:)))
        
        dealView.addGestureRecognizer(tap)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }
    func onViewTapped(_ sender: UITapGestureRecognizer) {
        let dealItemViewController = self.storyboard?.instantiateViewController(withIdentifier:
            "DealItemViewController") as! DealItemViewController
        dealItemViewController.deal = DealsModel(dealId: dealsModel[self.slideShow.currentPage].mIdDeal, titleDeal: dealsModel[self.slideShow.currentPage].mTitleDeal, productName: dealsModel[self.slideShow.currentPage].mProductName, description: dealsModel[self.slideShow.currentPage].mDescriptionDeal, price: dealsModel[self.slideShow.currentPage].mPriceDeal, image: dealsModel[self.slideShow.currentPage].mImageUrlDeal, categoryId: 0)
        self.navigationController?.pushViewController(dealItemViewController, animated: true)

    }
    func setDealsInfo () -> Void {
        self.titleDeal.text = self.dealsModel[self.slideShow.currentPage].mTitleDeal
        self.descriptionDeal.text = self.dealsModel[self.slideShow.currentPage].mDescriptionDeal
        self.priceDeal.text = "₪" + String(format: "%.2f",self.dealsModel[self.slideShow.currentPage].mPriceDeal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    func initializeData() -> Void {
        self.buttonShowDeals.setTitle("order_now_button".localized(), for: .normal)
        self.feelingHungryLabel.text = "feeling_hungry_label".localized()
        self.title = "home_title".localized()
        self.navigationItem.backBarButtonItem?.title = "home_title".localized()
        self.tabBarItem.title = "home_title"
       
        self.tabBarController?.tabBar.items?[0].title = "home_title".localized()
        self.tabBarController?.tabBar.items?[1].title = "title_locator".localized()
        self.tabBarController?.tabBar.items?[2].title = "deals_title".localized()
        self.tabBarController?.tabBar.items?[3].title = "account_title".localized()
        self.tabBarController?.tabBar.items?[4].title = "basket_title".localized()

        
    }
    
    
    
}

