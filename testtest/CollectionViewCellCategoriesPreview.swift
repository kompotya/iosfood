//
//  CollectionViewCellCategoriesPreview.swift
//  testtest
//
//  Created by Michael Ovsienko on 25.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellCategoriesPreview: UICollectionViewCell {
    
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var TextPreview: UILabel!
}
