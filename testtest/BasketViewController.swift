//
//  BasketViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 27.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Toaster
class BasketViewController: UIViewController,  UICollectionViewDelegate, UICollectionViewDataSource {

   
    @IBOutlet weak var clearBasketButton: UIButton!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var bastekTitleLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    var textNotificationPositionX : CGFloat = 0
    var textNotificationPositionY : CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundImage() 
        
        self.CollectionView.dataSource = self
        self.CollectionView.delegate = self
        textNotificationPositionX = totalPrice.frame.origin.x
        textNotificationPositionY = totalPrice.frame.origin.y
        
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
        self.clearBasketButton.isHidden = true
        var totalPriceItems : Double = 0
        self.CollectionView.reloadData()
        if (Singleton.sharedInstance.listBasketItems.count != 0 ){
            self.clearBasketButton.isHidden = false
            for item in Singleton.sharedInstance.listBasketItems {
                totalPriceItems = totalPriceItems + item.priceItem*Double(item.countItem)
            }
            totalPrice.text = "total_price".localized()  + "₪" + String(format: "%.2f",totalPriceItems)
            totalPrice.frame.origin.x = textNotificationPositionX
            totalPrice.frame.origin.y = textNotificationPositionY
        }
        else {
            totalPrice.text = "basket_empty".localized()
            totalPrice.center = self.view.center
            totalPrice.center.x = self.view.center.x
            totalPrice.center.y = self.view.center.y
        }

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Singleton.sharedInstance.listBasketItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_basket_item", for: indexPath) as? CollectionViewCellBasketItem
        cell?.basketItemTitle.text = Singleton.sharedInstance.listBasketItems[indexPath.row].titleItem
        cell?.basketItemDescription.text = Singleton.sharedInstance.listBasketItems[indexPath.row].descriptionItem
        cell?.basketItemImage.image = Singleton.sharedInstance.listBasketItems[indexPath.row].imageItem
        cell?.basketItemPrice.text = "₪" + String(format: "%.2f",Singleton.sharedInstance.listBasketItems[indexPath.row].priceItem)
        cell?.basketItemCount.text = "count".localized() + String(Singleton.sharedInstance.listBasketItems[indexPath.row].countItem)
        setCustomLayer(cell: cell!)
        
        return cell!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCustomLayer (cell : CollectionViewCellBasketItem){
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true;
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false

    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    @IBAction func clearBasket(_ sender: Any) {
        if Singleton.sharedInstance.listBasketItems.count != 0 {
            Singleton.sharedInstance.listBasketItems = [BasketItem]()
            UserDefaults.standard.set([BasketItem](), forKey: "basketItems")
            self.CollectionView.reloadData()
            self.clearBasketButton.isHidden = true
            totalPrice.text = "basket_empty".localized()
            self.tabBarController?.tabBar.items![4].badgeValue = nil           
            Toast(text: "clear_basket".localized(), duration: 0.5).show()
            totalPrice.center = self.view.center
            totalPrice.center.x = self.view.center.x
            totalPrice.center.y = self.view.center.y
        }
    }
    func initializeData() -> Void {
        bastekTitleLabel.text = "basket_title".localized()
        self.tabBarItem.title = "basket_title".localized()
        self.clearBasketButton.setTitle("basket_clearing".localized(), for: .normal)
    }
}
