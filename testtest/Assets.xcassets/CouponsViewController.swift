//
//  CouponsViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 20.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import XLPagerTabStrip
class CouponsViewController: UIViewController, IndicatorInfoProvider, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    var couponModel = [CouponsModel]()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        doGetRequest()
        setBackgroundImage() 
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }

    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "coupons".localized())
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return couponModel.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_view_cell_coupons", for: indexPath) as! CollectionViewCellCoupons
        if couponModel.count != 0 {
            cell.titleCoupon.text = couponModel[indexPath.row].titleCoupon
            cell.descriptionCoupon.text = couponModel[indexPath.row].descriptionCoupon
            cell.expDateCoupon.text = "תוקף:" + (couponModel[indexPath.row].expirationDateCoupon)
            cell.codeCoupon.text = "קוד קופון:"+String(couponModel[indexPath.row].couponId)
            
            let url = URL(string: couponModel[indexPath.row].imageCoupon)
            cell.imageCoupon.sd_setImage(with: url)
            cell.imageCoupon.setShowActivityIndicator(true)
            cell.imageCoupon.setIndicatorStyle(.gray)
        }
        
        
        
        return cell
    }

    func doGetRequest () ->Void{
        let parameters: Parameters = ["customerId": 1]
        
        Alamofire.request(Constants.API_URL_SCREEN_DATA, method: .get, parameters: parameters).responseJSON {response  in
            let jsonResponse = (response.result.value) as? [String:AnyObject]
            guard let coupons = jsonResponse?["coupons"] as? [AnyObject] else {
                print("error")
                return
            }
            for coupon in coupons{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                if let date = dateFormatter.date(from: (coupon["expirationDate"] as? String)!) {
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.couponModel.append(CouponsModel(id: (coupon["code"] as? String)!, title: (coupon["title"] as? String)!, description: (coupon["description"] as? String)!, expDate: dateFormatter.string(from: date), image: (coupon["img1x"] as? String)!))
                }
            
            }
            
            self.CollectionView.reloadData()
            
        }
        
        
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    func initializeData (){
        titleLabel.text = "coupons_title".localized()
    }

   
}
