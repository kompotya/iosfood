//
//  Coupons.swift
//  testtest
//
//  Created by Michael Ovsienko on 20.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class CouponsModel {
    var couponId : String
    var titleCoupon : String
    var descriptionCoupon : String
    var expirationDateCoupon  : String
    var imageCoupon : String
    init(id : String, title : String, description : String, expDate : String, image : String) {
        self.couponId = id
        self.titleCoupon = title
        self.descriptionCoupon = description
        self.expirationDateCoupon = expDate
        self.imageCoupon = image
    }
}
