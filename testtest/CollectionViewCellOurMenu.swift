//
//  CollectionViewCellOurMenu.swift
//  testtest
//
//  Created by Michael Ovsienko on 18.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellOurMenu: UICollectionViewCell {
    
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var titleCategory: UILabel!
    @IBOutlet weak var descriptionCategory: UILabel!
}
