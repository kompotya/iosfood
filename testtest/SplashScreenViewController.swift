//
//  SplashScreenViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 23.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Alamofire
class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var dealsModel = [ThreeDealModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "phoneNumber") == nil {
            let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            self.present(loginViewController!, animated: true, completion: nil)
        } else {
            doGetRequest()
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doGetRequest () ->Void{
        
        Alamofire.request(Constants.API_URL_THREE_DEALS, method: .get, parameters: nil).responseJSON { (response) in
            var image : String;
            let jsonResponse = (response.result.value) as? [String:AnyObject]
            
            if let bestDeal = jsonResponse?["bestDeal"] as? [String:AnyObject]{
                let title = bestDeal["title"] as? String
                let description = bestDeal["descr"] as? String
                var productName = bestDeal["productName"] as? String
                productName = productName?.replacingOccurrences(of: "<span>", with: "")
                productName = productName?.replacingOccurrences(of: "</span>", with: "")
                let price = bestDeal["price"] as? Double
                let productNameSecondary = bestDeal["productNameSecondary"] as? String
                image = (bestDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: (bestDeal["id"] as? Int)!,imageDeal: image, titleDeal: title!, descriptionDeal: description!, priceDeal: price!, productNameDeal :productName!, productNameSecondaryDeal: productNameSecondary!))
            }
            if let contextDeal = jsonResponse?["contextDeal"] as? [String:AnyObject]{
                let title = contextDeal["title"] as? String
                let description = contextDeal["descr"] as? String
                let productName = contextDeal["productName"] as? String
                let price = contextDeal["price"] as? Double
                let productNameSecondary = contextDeal["productNameSecondary"] as? String
                image = (contextDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: contextDeal["id"] as! Int,imageDeal: image, titleDeal: title!, descriptionDeal: description!, priceDeal: price!, productNameDeal :productName!, productNameSecondaryDeal: productNameSecondary!))
            }
            if let todaysDeal = jsonResponse?["todaysDeal"] as? [String:AnyObject]{
                let title = todaysDeal["title"] as? String
                let description = ""
                var productNameToday = todaysDeal["productName"] as? String
                productNameToday = productNameToday?.replacingOccurrences(of: "<span>", with: "")
                productNameToday = productNameToday?.replacingOccurrences(of: "</span>", with: "")
                
                let price = todaysDeal["price"] as? Double
                let productNameSecondary = todaysDeal["productNameSecondary"] as? String
                image = (todaysDeal["img1x"] as? String)!
                image = image.replacingOccurrences(of: " ", with: "%20")
                self.dealsModel.append(ThreeDealModel(id: todaysDeal["id"] as! Int,imageDeal: image, titleDeal: title!, descriptionDeal: description, priceDeal: price!, productNameDeal :productNameToday!, productNameSecondaryDeal: productNameSecondary!))
            }
            
            self.activityIndicator.stopAnimating()
            
            Singleton.sharedInstance.listModelsData = self.dealsModel
            let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabConttrollerViewController")
            
            self.present(tabViewController!, animated: true, completion: nil)
            
            
        }
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
