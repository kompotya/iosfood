//
//  CollectionViewCellDeals.swift
//  testtest
//
//  Created by Michael Ovsienko on 18.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellDeals: UICollectionViewCell {
    
    @IBOutlet weak var imageDeal: UIImageView!
    @IBOutlet weak var titleDeal: UILabel!
    @IBOutlet weak var descriptionDeal: UILabel!
    @IBOutlet weak var priceDeal: UILabel!
}
