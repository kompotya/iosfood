//
//  Constants.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class Constants{
    static let API_URL_THREE_DEALS : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/three-deals"
    static let API_URL_DEALS : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/deals"
    static let API_URL_CATEGORIES : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/menu-categories/menu-categories"
    static let API_URL_DISHES : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/menu/by-category"
    static let API_URL_SCREEN_DATA : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/profile/screenData"
    static let GOOGLE_MAPS_API_KEY : String = "AIzaSyAYJfghON2g-QwHHBOy1HbJIDimTCEu0Mc"
    static let API_URL_UPDATE_CUSTOMER : String = "http://vneto-mocks-dev.azurewebsites.net/kiosk/profile/update-customer"
    static let API_UTL_CUSTOMER_BY_PHONE : String =  "http://vneto-mocks-dev.azurewebsites.net/kiosk/customer-by-phone"
    
}
