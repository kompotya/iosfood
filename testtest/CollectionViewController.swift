//
//  CollectionViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit


class CollectionViewController: UICollectionViewController {

    var names: Array = [String]()  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        names = ["aas","bbb","ccc"]
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return names.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_cell", for: indexPath)
        
        // Configure the cell
    
        return cell
    }
    func doGetRequest () ->Void{
        var cityModels = [CitiesModel]()
        
        let urlString = Constants.API_URL
        
        
        guard let url  = URL(string: urlString ) else {
            print("error convert to url")
            return;
        }
        
        let urlRequest = URLRequest(url: url)
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: AnyObject] else {
                        print("error trying to convert data to JSON")
                        return
                }
                
                if let cities = todo["cities"] as? [[String:AnyObject]] {
                    for city in cities {
                        let cityName = city["name_ru"] as? String
                        let cityId = city["city_id"] as? Int
                        let cityEmblem = city["emblem"] as? String
                        cityModels.append(CitiesModel(cityName: cityName!, cityId: cityId! , cityEmblem : cityEmblem!))
                    }
                    Singleton.sharedInstance.listModelsData = cityModels
                    
                }
                self.showResult()
                
            }
                
            catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    func showResult()-> Void {
        for cit in Singleton.sharedInstance.listModelsData {
            print("cityId = \(cit.mCityId)  nameCity = \(cit.mCityName) + emblem = \(cit.mCityEmblem)")
        }
    }

   
}
