//
//  CollectionViewCell.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleDeal: UILabel!
    @IBOutlet weak var priceDeal: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var descriptionDeal: UILabel!
    @IBOutlet weak var imageDeal: UIImageView!
    @IBOutlet weak var productNameSecondary: UILabel!
    
}
