//
//  TabConttrollerViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 23.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class TabConttrollerViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = .white
        self.tabBar.barTintColor = UIColor(colorLiteralRed: 37.0/255.0, green: 39.0/255.0, blue: 42.0/255.0, alpha: 1)
        // Do any additional setup after loading the view.
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.lightGray, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: 100, height: 100) 
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

