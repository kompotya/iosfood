//
//  CitiesModel.swift
//  testtest
//
//  Created by Michael Ovsienko on 11.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class ThreeDealModel {
    var mIdDeal : Int
    var mImageUrlDeal :String
    var mTitleDeal : String
    var mDescriptionDeal : String
    var mPriceDeal : Double
    var mProductName : String
    var mProductNameSecondary : String
    
    init(id : Int, imageDeal : String, titleDeal : String, descriptionDeal : String, priceDeal : Double, productNameDeal : String ,productNameSecondaryDeal : String) {
        self.mIdDeal = id
        self.mImageUrlDeal = imageDeal
        self.mTitleDeal = titleDeal
        self.mDescriptionDeal = descriptionDeal
        self.mPriceDeal = priceDeal
        self.mProductName = productNameDeal
        self.mProductNameSecondary = productNameSecondaryDeal
    }
    
    
}
