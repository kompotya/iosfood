//
//  Category.swift
//  testtest
//
//  Created by Michael Ovsienko on 18.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
class DealsModel {
    var mDealId : Int
    var mTitleDeal : String
    var mProductName : String
    var mDescriptionDeal : String
    var mPriceDeal : Double
    var mImageDeal : String
    var mCategoryID  : Int
    init(dealId: Int, titleDeal : String, productName : String, description : String, price : Double, image : String, categoryId : Int ) {
        mDealId = dealId
        mTitleDeal = titleDeal
        mProductName = productName
        mDescriptionDeal = description
        mPriceDeal = price
        mImageDeal = image
        mCategoryID = categoryId
    }
}
