//
//  PersonalZoneViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 20.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import XLPagerTabStrip
import Toaster
class PersonalZoneViewController: UIViewController, UITextFieldDelegate,IndicatorInfoProvider {
    
    
    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleWelcomeLabel: UILabel!
    @IBOutlet weak var titleImportantLabel: UILabel!
    @IBOutlet weak var editDataButton: UIButton!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var nameUser: UITextField!
    @IBOutlet weak var phoneUser: UITextField!
    @IBOutlet weak var phoneUserLabel: UILabel!
    @IBOutlet weak var emailUser: UITextField!
    @IBOutlet weak var emailUserLabel: UILabel!
    @IBOutlet weak var addressUser: UITextField!
    @IBOutlet weak var addressUserLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var mainView: UIView!
    
    
    //MARK: - Variables
    var customerModel : Customer? = nil
    var isEditind : Bool = false
    
    
    //MARK: - Outlets action
    @IBAction func changeUserDate(_ sender: Any) {
        if !isEditing {
            isEditing = true
            self.editDataButton.setTitle("personal_zone_save_button".localized(), for: .normal)
            self.setSwipeGestureEnabled(isEnabled: false)
            self.setTextFieldEditingEnable(isEnabled: true)
            self.nameUser.becomeFirstResponder()
            scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
            
            
        } else {
            isEditing = false
            self.setTextFieldEditingEnable(isEnabled: false)
            self.setSwipeGestureEnabled(isEnabled: true)
            self.updateUserData()
            self.editDataButton.setTitle("personal_zone_edit_button".localized(), for: .normal)
        }
    }

    
    
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        doGetRequest()
        setBackgroundImage()
        setTextFieldEditingEnable(isEnabled: false)
    }
   
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
        self.initializeData()
    }
    
    //MARK: Other functions
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "personal".localized())
    }
    
    func doGetRequest () ->Void{
        let customerId = UserDefaults.standard.object(forKey: "customerId") as? Int
        let parameters: Parameters = ["customerId": customerId]
        
        Alamofire.request(Constants.API_URL_SCREEN_DATA, method: .get, parameters: parameters).responseJSON {response  in
            let jsonResponse = (response.result.value) as? [String:AnyObject]
            guard let customer = jsonResponse?["customerData"] as? [String:AnyObject] else {
                print("error")
                return
            }
        
            let phone = customer["phone"] as? String
            self.phoneUser.text = phone
            let fullName = customer["fullName"] as? String
            self.nameUser.text  = fullName
            let email = customer["email"] as? String
            self.emailUser.text = email
            let address = customer["address"] as? String
            self.addressUser.text = address
    
        }
        
        
    }
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    func setTextFieldEditingEnable (isEnabled : Bool){
        nameUser.isUserInteractionEnabled = isEnabled
        phoneUser.isUserInteractionEnabled = isEnabled
        emailUser.isUserInteractionEnabled = isEnabled
        addressUser.isUserInteractionEnabled = isEnabled
        

        
        
    }
    func setSwipeGestureEnabled(isEnabled : Bool){
        for view in (self.parent?.view.subviews)! {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = isEnabled
            }
        }
    }
    
    func updateUserData (){
        let customerId = UserDefaults.standard.object(forKey: "customerId") as? Int

        let customerData : Customer = Customer(id: customerId!, name:  self.nameUser.text!, phone: self.phoneUser.text!, email: self.emailUser.text!, address: self.addressUser.text!)
        Alamofire.request(Constants.API_URL_UPDATE_CUSTOMER, method: .patch, parameters: customerData.getParameters(), encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
            
            switch response.result {
            case .success:
                Toast(text: "data_change_succesful".localized(), duration: 0.5).show()
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.layer.position.y > self.view.center.y {
            scrollView.setContentOffset(CGPoint(x: 0,y: 250), animated: true)
        }
        
        textField.layer.masksToBounds = true
        textField.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        textField.layer.borderWidth = 2.0
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
        textField.layer.masksToBounds = false
        textField.layer.borderWidth = 0.0
    }
    func initializeData() -> Void {
        titleLabel.text = "personal_zone_title".localized()
        titleImportantLabel.text = "personal_welcome_label".localized()
        titleWelcomeLabel.text = "personal_about_user_label".localized()
        nameUserLabel.text = "personal_name".localized()
        phoneUserLabel.text = "personal_phone".localized()
        emailUserLabel.text = "personal_email".localized()
        addressUserLabel.text = "personal_address".localized()
        editDataButton.setTitle("personal_zone_edit_button".localized(), for: .normal)
        self.title = "personal_zone_title"
    }
    
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
