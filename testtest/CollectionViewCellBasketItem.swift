//
//  CollectionViewCellBasketItem.swift
//  testtest
//
//  Created by Michael Ovsienko on 30.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class CollectionViewCellBasketItem: UICollectionViewCell {
    
    @IBOutlet weak var basketItemPrice: UILabel!
    @IBOutlet weak var basketItemDescription: UILabel!
    @IBOutlet weak var basketItemTitle: UILabel!
    @IBOutlet weak var basketItemImage: UIImageView!
    @IBOutlet weak var basketItemCount: UILabel!
}
