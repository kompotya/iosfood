//
//  SettingsViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 25.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Localize_Swift
import DropDown
import Toaster
class SettingsViewController: UIViewController,  IndicatorInfoProvider {
    
    
    @IBOutlet weak var settingsTitle: UILabel!
    @IBOutlet weak var chooseLanguageTitle: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var chooseLanguageButton: UIButton!
    
    var pickerData: [String] = [String]()
    var languageData : [String] = [String]()
    
    let languageDropDown = DropDown()
    
    
    
    @IBAction func showDropDown(_ sender: Any) {
        languageDropDown.show()
    }
    
    
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "settings".localized())
    }
    override func viewDidLoad() {
        pickerData = ["English", "Украинский"]
        languageData = ["en", "uk"]
        
        super.viewDidLoad()
        
        
        languageDropDown.anchorView = chooseLanguageButton
        
        languageDropDown.bottomOffset = CGPoint(x: 0, y: chooseLanguageButton.bounds.height)
        
        languageDropDown.dataSource = pickerData
        
        
        
        customizeDropDown(self)
        
        
        languageDropDown.selectionAction = { [unowned self] (index, item) in
            Localize.setCurrentLanguage(self.languageData[index])
            self.initializeData()
            Toast(text: "language_changed_succesfull".localized(), duration: 0.5).show()

        }
        
        setBackgroundImage()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.initializeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func actionLogout(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "phoneNumber")
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        self.present(loginViewController!, animated: true, completion: nil)
    }
    
    
    
    func setBackgroundImage()  {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    func initializeData() -> Void {
        settingsTitle.text = "settings".localized()
        chooseLanguageTitle.text = "language_label".localized()
        logoutButton.setTitle("logout_button".localized(), for: .normal)
        chooseLanguageButton.setTitle("language_label".localized(), for: .normal)
        self.tabBarController?.tabBar.items?[0].title = "home_title".localized()
        self.tabBarController?.tabBar.items?[1].title = "title_locator".localized()
        self.tabBarController?.tabBar.items?[2].title = "deals_title".localized()
        self.tabBarController?.tabBar.items?[3].title = "account_title".localized()
        self.tabBarController?.tabBar.items?[4].title = "basket_title".localized()
        
    }
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.5)
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

