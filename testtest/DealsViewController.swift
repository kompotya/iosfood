//
//  DealsViewController.swift
//  testtest
//
//  Created by Michael Ovsienko on 17.01.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow
import Alamofire
class DealsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
 
 @IBOutlet weak var titleDeal: UILabel!
 @IBOutlet weak var slideShow: ImageSlideshow!
 @IBOutlet weak var priceDeal: UILabel!
 @IBOutlet weak var descriptionDeal: UILabel!
 @IBOutlet weak var btnMenu: UIBarButtonItem!
 @IBOutlet weak var ourMenuLabel: UILabel!
 
 @IBOutlet weak var dealsView: UIView!
 @IBOutlet weak var collectionViewCategories: UICollectionView!
 
 var dealsModel = [DealsModel]()
 var categoriesModel = [CategoryModel]()
 
 
 
 var imagesForSlideshow = [SDWebImageSource]()
 override func viewDidLoad() {
  super.viewDidLoad()
  doGetRequest()
  setBackgroundImage()
  
  self.collectionViewCategories.dataSource = self
  self.collectionViewCategories.delegate = self
  slideShow.slideshowInterval = 4
  slideShow.backgroundColor = UIColor.clear
  slideShow.pageControl.currentPageIndicatorTintColor = UIColor.red;
  slideShow.pageControl.pageIndicatorTintColor = UIColor.white;
  slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
  self.navigationController?.setNavigationBarHidden(true, animated: true)
  
  let tap = UITapGestureRecognizer(target: self, action: #selector(self.onViewTapped(_:)))
  
  dealsView.addGestureRecognizer(tap)
 }
 
 override func viewWillAppear(_ animated: Bool) {
  self.initializeData()
 }
 func onViewTapped(_ sender: UITapGestureRecognizer) {
  let dealItemViewController = self.storyboard?.instantiateViewController(withIdentifier:
   "DealItemViewController") as! DealItemViewController
  dealItemViewController.deal = self.dealsModel[self.slideShow.currentPage]
  self.navigationController?.pushViewController(dealItemViewController, animated: true)
  
 }
 func numberOfSections(in collectionView: UICollectionView) -> Int {
  return categoriesModel.count / 2
 }
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
  return 2
 }
 
 func collectionView(_ collectionViewCategories: UICollectionView, cellForItemAt indexPath: IndexPath) ->UICollectionViewCell {
  let cell = collectionViewCategories.dequeueReusableCell (withReuseIdentifier: "cell_menu_categories", for: indexPath as IndexPath) as! CollectionViewCellCategoriesPreview
  
  let index = indexPath.section * 2 + indexPath.row
  cell.TextPreview.text = categoriesModel[index].mTitleCategory
  let url = URL(string: categoriesModel[index].mImageCategory)
  cell.imagePreview.sd_setImage(with: url)
  cell.imagePreview.setShowActivityIndicator(true)
  cell.imagePreview.setIndicatorStyle(.gray)
  cell.layer.cornerRadius = 5.0
  cell.layer.borderWidth = 2.0
  cell.layer.borderColor = UIColor.clear.cgColor
  cell.layer.masksToBounds = true
  return cell
 }
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
  let dishesViewController = self.storyboard?.instantiateViewController(withIdentifier:
   "DishesViewController") as! DishesViewController
  let index = indexPath.section * 2 + indexPath.row
  print(index)
  dishesViewController.categoryID = categoriesModel[index].mIdCategory
  dishesViewController.categoryName = categoriesModel[index].mTitleCategory
  self.navigationController?.pushViewController(dishesViewController, animated: true)
 }
 
 override func didReceiveMemoryWarning() {
  super.didReceiveMemoryWarning()
  // Dispose of any resources that can be recreated.
 }
 
 func doGetRequest () ->Void{
  Alamofire.request(Constants.API_URL_CATEGORIES, method: .get, parameters: nil).responseJSON {response  in
   let jsonResponse = (response.result.value) as? [AnyObject]
   for category in jsonResponse!{
    self.categoriesModel.append(CategoryModel(title: (category["title"] as? String)!, description: (category["description"] as? String)!, image: (category["img"] as? String)!  , id: (category["category"] as? String)!))
   }
   self.collectionViewCategories.reloadData()
   
  }
  Alamofire.request(Constants.API_URL_DEALS, method: .get
   , parameters: nil).responseJSON{
    response in
    switch response.result {
    case .success:
     let jsonResponse = (response.result.value) as? [AnyObject]
     for deal in jsonResponse! {
      
      self.dealsModel.append(DealsModel(dealId: deal["id"] as! Int, titleDeal: deal["title"] as! String, productName: deal["productName"] as! String, description: deal["description"] as! String, price: deal["priceBase"] as!Double, image: deal["img1x"] as! String, categoryId: deal["categoryId"] as! Int))
      self.imagesForSlideshow.append(SDWebImageSource(urlString: deal["img1x"] as! String!)!)
     }
     self.slideShow.setImageInputs(self.imagesForSlideshow)
     self.setDealsProperties()
     
     Timer.scheduledTimer(timeInterval: 0.1,
                          target: self,
                          selector: #selector(self.setDealsProperties),
                          userInfo: nil,
                          repeats: true)
     break
    case .failure(let error):
     print(error)
     break
     
    }
  }
 }
 
 func setDealsProperties (){
  self.titleDeal.text = self.dealsModel[self.slideShow.currentPage].mProductName
  self.descriptionDeal.text = self.dealsModel[self.slideShow.currentPage].mDescriptionDeal
  self.priceDeal.text = "₪" + String(format: "%.2f", self.dealsModel[self.slideShow.currentPage].mPriceDeal)
 }
 func setBackgroundImage()  {
  UIGraphicsBeginImageContext(self.view.frame.size)
  UIImage(named: "background")?.draw(in: self.view.bounds)
  let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
  UIGraphicsEndImageContext()
  self.view.backgroundColor = UIColor(patternImage: image)
 }
 
 func initializeData() -> Void {
  self.navigationItem.backBarButtonItem?.title = "deals_title".localized()
  self.ourMenuLabel.text = "our_menu_title".localized()
  self.tabBarItem.title = "deals_title"
 }
 
 /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
 
}
